package be.matthiasdepoorter.model;

import java.util.Random;

public class PizzaShop extends Thread {

	private int timeSleep;
	private PizzaWarehouse pizzaWarehouse;
	private Pizza orderedPizza;
	private Pizza receivedPizza;
	private Random rand = new Random();

	public PizzaShop(int timeSleep, PizzaWarehouse pw) {
		this.timeSleep = timeSleep;
		this.pizzaWarehouse = pw;
	}

	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(timeSleep);
			} catch (InterruptedException e) {

			}
			orderedPizza = new Pizza(PizzaType.values()[rand.nextInt(PizzaType.values().length)]);
			receivedPizza = this.pizzaWarehouse.getPizza(orderedPizza);
			if (orderedPizza.compareTo(receivedPizza) == 0) {
				System.out.println(Thread.currentThread().getName() + " ordered " + orderedPizza.toString() + ", got "
						+ receivedPizza.toString() + "; correct order");
			} else {
				System.out.println(Thread.currentThread().getName() + " ordered " + orderedPizza.toString() + ", got "
						+ receivedPizza.toString() + "; incorrect order, half price");
				this.pizzaWarehouse.checkStock();
			}
		}
	}
}
