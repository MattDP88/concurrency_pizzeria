package be.matthiasdepoorter.model;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PizzeriaApp {

	public static void main(String[] args) {
		PizzaWarehouse pw = new PizzaWarehouse();
		PizzaShop shop1 = new PizzaShop(1000, pw);
		PizzaShop shop2 = new PizzaShop(3000, pw);
		PizzaShop shop3 = new PizzaShop(6000, pw);
		PizzaFactory factory1 = new PizzaFactory(pw);
		PizzaFactory factory2 = new PizzaFactory(pw);
//		shop1.start();
//		shop2.start();
//		shop3.start();
//		new Thread(factory1).start();
//		new Thread(factory2).start();
		
		ExecutorService executor = Executors.newFixedThreadPool(5);
		executor.execute(shop1);
		executor.execute(shop2);
		executor.execute(shop3);
		executor.execute(factory1);
		executor.execute(factory2);
		executor.shutdown();
		
	}

}
