package be.matthiasdepoorter.model;

public enum PizzaType {

	MARGHERITA,
	QUATTROFORMAGGI,
	FUNGHI,
	QUATTROSTAGIONI;
	
}
