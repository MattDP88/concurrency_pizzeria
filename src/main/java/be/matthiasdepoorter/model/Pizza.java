package be.matthiasdepoorter.model;

public class Pizza implements Comparable<Pizza>{
	
	private PizzaType pizzaType;
	
	public Pizza(PizzaType p){
		this.pizzaType=p;
	}
	
	public PizzaType getPizzaType(){
		return this.pizzaType;
	}
	
	@Override
	public String toString(){
		return "Pizza " + this.pizzaType.toString();
	}

	@Override
	public int compareTo(Pizza e) {
		return this.pizzaType.compareTo(e.pizzaType);
	}
	
}
