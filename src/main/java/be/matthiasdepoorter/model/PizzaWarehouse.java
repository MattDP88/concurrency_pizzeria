package be.matthiasdepoorter.model;

import java.util.*;
import java.util.concurrent.*;

public class PizzaWarehouse {

	private Map<PizzaType, LinkedList<Pizza>> pizzaMap = new ConcurrentHashMap<>();
	private AbstractQueue<PizzaType> emptyPizzaTypeList = new ConcurrentLinkedQueue<>();
	private Object shopMonitor = new Object();
	private Object factoryMonitor = new Object();
	private boolean wareHouseEmpty = false;
	private Random rand = new Random();

	public PizzaWarehouse() {
		for (PizzaType p : PizzaType.values()) {
			pizzaMap.put(p, new LinkedList<Pizza>());
		}
		this.checkStock();
	}

	// Consumer method
	public Pizza getPizza(Pizza p) {
		synchronized (shopMonitor) {
			while (wareHouseEmpty) {
				try {
					shopMonitor.wait();
				} catch (InterruptedException e) {
				}
			}
			for (PizzaType e : PizzaType.values()) {
				System.out.print(e.toString() + ": " + this.pizzaMap.get(e).size() + "\t");
			}
			System.out.println();

			if (!this.pizzaMap.get(p.getPizzaType()).isEmpty()) {
				shopMonitor.notifyAll();
				return this.pizzaMap.get(p.getPizzaType()).removeFirst();
			} else {
				boolean tempBoolean = true;
				int temp = rand.nextInt(pizzaMap.size());
				shopMonitor.notifyAll();
				while (tempBoolean) {
					if (!this.pizzaMap.get(PizzaType.values()[temp]).isEmpty()) {
						tempBoolean = false;
						return this.pizzaMap.get(PizzaType.values()[temp]).removeFirst();
					} else {
						temp = rand.nextInt(pizzaMap.size());
					}
				}
			}

		}
		return null;
	}

	public void checkStock() {
		for (PizzaType p : PizzaType.values()) {
			if (pizzaMap.get(p).isEmpty()) {
				this.emptyPizzaTypeList.add(p);
			}
		}

		if (this.emptyPizzaTypeList.size() >= (double) this.pizzaMap.size() / 2) {
			this.wareHouseEmpty = true;
			
			synchronized (shopMonitor){
				shopMonitor.notifyAll();
			}
			synchronized (factoryMonitor) {
				factoryMonitor.notifyAll();
			}
			
		} else {
			this.emptyPizzaTypeList.clear();
			System.out.println("Stock checked, not necessary to fill");
		}
	}

	// Producer method
	public void addPizza() {
		synchronized (factoryMonitor) {
			while (!wareHouseEmpty) {
				try {
					factoryMonitor.wait();
				} catch (InterruptedException e) {
				}
			}
			for (PizzaType p : this.emptyPizzaTypeList) {
				for (int i = 0; i < 10; i++) {
					Pizza temp = new Pizza(p);
					this.pizzaMap.get(p).add(temp);
					factoryMonitor.notifyAll();
				}
				System.out.println(Thread.currentThread().getName() + " refilled " + p.toString());
				this.emptyPizzaTypeList.remove(p);
				
			}
			
		}

		if (this.emptyPizzaTypeList.size() == 0) {
			this.wareHouseEmpty = false;
			synchronized (shopMonitor) {
				shopMonitor.notifyAll();
			}

		}
	}
}
