package be.matthiasdepoorter.model;

public class PizzaFactory implements Runnable {

	private PizzaWarehouse pizzaWarehouse;

	public PizzaFactory(PizzaWarehouse pw) {
		this.pizzaWarehouse = pw;
	}

	@Override
	public void run() {
		while (true) {
			this.pizzaWarehouse.addPizza();
		}
	}
}
